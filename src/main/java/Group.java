import lombok.Data;

@Data
public class Group {
    private int id;
    public String name;

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}