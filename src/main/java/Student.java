import lombok.Data;

@Data
public class Student {
    private String id;
    private String groupId;
    public String name;
    public String phone;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
